import urllib
import re
import requests

import client
import dom_parser
import cleantitle
import source_utils

def cleanhtml(raw_html):
  cleanr = re.compile('<.*?>')
  cleantext = re.sub(cleanr, '', raw_html)
  return cleantext


def search(titles, year, site, titleRegex):
    result = Google(titles, year, site, titleRegex)
    if result:
        return result
    return DuckDuckGo(titles, year, site, titleRegex)

def Google(titles, year, site, titleRegex):
    try:
        searchTerm = '%s site:%s' % (titles[0], site)
        url = 'https://www.google.de/search?q=' + searchTerm
        result = requests.get(url).content

        if cleantitle.get(searchTerm.lower()) in cleantitle.get(result.lower()):
            links = dom_parser.parse_dom(result, 'h3')
            links = getLinksAndTitle(links, site, titleRegex, year, titles)

            if len(links) > 0:
                return source_utils.strip_domain(re.findall("(http.*?)&", links[0])[0])

        return
    except:
        return

def DuckDuckGo(titles, year, site, titleRegex):
    try:
        params = {
            'q': '%s site:%s' % (titles[0], site),
            'kl': 'de_DE'
        }

        r = requests.get('https://duckduckgo.com/html', params, headers={'Content-Type': 'application/x-www-form-urlencoded'})
        result = r.content

        if cleantitle.get(params['q'].lower()) in cleantitle.get(result.lower()):
            links = dom_parser.parse_dom(result, 'h2', attrs={'class': 'result__title'})
            links = getLinksAndTitle(links, site, titleRegex, year, titles)

            if len(links) > 0:
                return source_utils.strip_domain(re.findall("(http.*)", links[0])[0])

        return
    except:
        return

def getLinksAndTitle(links, site, titleRegex, year, titles):
    links = dom_parser.parse_dom(links, 'a')
    links = [(client.replaceHTMLCodes(i.content), i.attrs['href']) for i in links if site in i.attrs['href']]

    links = [(re.findall(titleRegex, i[0])[0], i[1]) for i in links if re.search(titleRegex, i[0]) is not None]
    links = [(cleanhtml(i[0]), i[1], year in i[1]) for i in links]

    links = sorted(links, key=lambda i: i[2], reverse=True)  # with year > no year

    t = [cleantitle.get(i) for i in set(titles) if i]
    return [urllib.unquote(i[1]) for i in links if cleantitle.get(i[0]) in t]
