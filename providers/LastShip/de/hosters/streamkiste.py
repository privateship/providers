# -*- coding: utf-8 -*-
import re
import urlparse

from providerModules.LastShip import cache
import cfscrape
from providerModules.LastShip import dom_parser
from providerModules.LastShip import source_utils
from providerModules.LastShip.recaptcha import recaptcha_app


class source:
    def __init__(self):
        self.priority = 1
        self.language = ['de']
        self.domains = ['streamkiste.tv']
        self.base_link = 'https://streamkiste.tv'
        self.search_link = self.base_link + '/include/fetch.php'

        self.year_link = '/jahr/%d.html'
        self.type_link = '/%s.html'
        self.scraper = cfscrape.create_scraper()

        self.recapInfo = ""

    def movie(self, imdb, title, localtitle, aliases, year):
        try:
            url = self.__search(imdb)
            return url, self.__getParameter(url)
        except:
            return

    def tvshow(self, imdb, tvdb, tvshowtitle, localtvshowtitle, aliases, year):
        try:
            url = self.__search(imdb)
            return url
        except:
            return

    def episode(self, url, imdb, tvdb, title, premiered, season, episode):
        try:
            if not url:
                return
            params = self.__getParameter(url)
            params[0]['season'] = season
            params[0]['episode'] = episode

            return url, params
        except:
            return

    def sources(self, url, hostDict, hostprDict):
        sources = []
        try:
            if not url:
                return sources
            url, params = url
            for param in params:
                content = cache.get(self.scraper.post, 8, urlparse.urljoin(self.base_link, url), data=param).content

                quali = dom_parser.parse_dom(content, 'div', attrs={'id': 'stream-h'})
                links = dom_parser.parse_dom(content, 'div', attrs={'id': 'stream'})

                for i in range(0, len(links)):
                    if 'mirror-lang' in quali[i].content and 'german' in dom_parser.parse_dom(quali[i], 'div', attrs={'id': 'mirror-lang'})[0].content:
                        quality = dom_parser.parse_dom(quali[i], 'div', attrs={'id': 'mirror-head'})[0].content
                        quality, info = source_utils.get_release_quality(quality)
                        info = "Recaptcha, " + info if info else "Recaptcha"

                        link = dom_parser.parse_dom(links[i], 'a')
                        link = [a.attrs for a in link if '1kino' not in a.attrs['href']]

                        for stream_link in link:
                            hoster = stream_link.pop('title')
                            hoster = re.findall("'(.*?)'", hoster)[0]
                            stream_link.pop('href')
                            stream_link.update(param)
                            stream_link = { str(k.replace('data-', '')): str(v) for k, v in stream_link.items()}

                            valid, hoster = source_utils.is_host_valid(hoster, hostDict)
                            if not valid: continue

                            sources.append({'source': hoster, 'quality': quality, 'language': 'de', 'url': (url, stream_link), 'info': info, 'direct': False, 'debridonly': False, 'checkquality': True, 'captcha': True})

            return sources
        except:
            return sources

    def resolve(self, url):
        try:
            url, params = url
            url = urlparse.urljoin(self.base_link, url)

            recap = recaptcha_app.recaptchaApp()

            key = recap.getSolutionWithDialog(url, "6LcGFzMUAAAAAJaE5lmKtD_Oi_YzC837_Nwt6Btv", self.recapInfo)
            print "Recaptcha2 Key: " + key
            response = None
            if key != "" and "skipped" not in key.lower():
                if 'serie' in url:
                    params.pop("ep")
                    params.pop("se")
                    params["frame"] = '2'
                    params["n94g57e25d"] = key
                    params["ceck"] = 'sk-stream'
                else:
                    params["token"] = key
                    params["req"] = '3'
                response = self.scraper.post(url, data=params, allow_redirects=False)
            elif not response or "skipped" in key.lower():
                return

            if response is not None:
                if response.status_code == 302:
                    return response.next.url
                else:
                    return dom_parser.parse_dom(response.content, 'a')[0].attrs['href']

        except:
            return

    def __search(self, imdb):
        try:
            content = cache.get(self.scraper.post, 8, self.search_link, data={'page': '1', 'type': 'search', 'sq': imdb}).content

            r = dom_parser.parse_dom(content, 'div', attrs={'class': 'movie-preview-content'})
            if len(r) == 1:
                return dom_parser.parse_dom(r, 'a')[0].attrs['href']
        except:
            return

    def __getParameter(self, link):
        content = cache.get(self.scraper.get, 8, urlparse.urljoin(self.base_link, link)).content

        option = dom_parser.parse_dom(content, 'option')
        pid = re.findall('pid:"(\d+)', content)[0]

        if 'data' in option[0].attrs:
            option = [i.attrs['data'] for i in option]
            return [{'pid': pid, 'req': re.findall('req:"(.*?)"', content)[0], 'rel': i} for i in option]
        else:
            return [{'pid': pid, 'ceck': re.findall('ceck:"(.*?)"', content)[0]}]


    def setRecapInfo(self, info):
        self.recapInfo = info