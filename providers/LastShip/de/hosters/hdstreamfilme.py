# -*- coding: utf-8 -*-

import urllib
import urlparse

from providerModules.LastShip import cache, client
from providerModules.LastShip import dom_parser
from providerModules.LastShip import source_utils
from providerModules.LastShip import cleantitle


class source:
    def __init__(self):
        self.priority = 1
        self.language = ['de']
        self.domains = ['hdstreamfilme.com']
        self.base_link = 'https://hdstreamfilme.com/'
        self.search = self.base_link + 'index.php?do=search&subaction=search&search_start=1&full_search=0&result_from=1&story=%s'

    def movie(self, imdb, title, localtitle, aliases, year):
        try:
            url = self.__search([localtitle] + aliases)
            if not url and title != localtitle:
                url = self.__search([title] + aliases)
            return url
        except:
            return

    def tvshow(self, imdb, tvdb, tvshowtitle, localtvshowtitle, aliases, year):
            return

    def episode(self, url, imdb, tvdb, title, premiered, season, episode):
            return

    def sources(self, url, hostDict, hostprDict):
        sources = []
        try:
            if not url:
                return sources
            sHtmlContent = cache.get(client.request, 48, urlparse.urljoin(self.base_link, url))
            links = dom_parser.parse_dom(sHtmlContent, "iframe")
            links = [i.attrs['src'] for i in links]

            for link in links:
                valid, host = source_utils.is_host_valid(link, hostDict)
                if not valid: continue

                sources.append({'source': host, 'quality': 'SD', 'language': 'de', 'url': link, 'direct': False, 'debridonly': False})

            if len(sources) == 0:
                raise Exception()
            return sources
        except:

            return sources

    def resolve(self, url):
        return url

    def __search(self, titles):
        try:
            t = [cleantitle.get(i) for i in set(titles) if i]
            url = self.search % urllib.quote_plus(titles[0])

            sHtmlContent = cache.get(client.request, 48, url)
            search_results = dom_parser.parse_dom(sHtmlContent, 'a', attrs={'class': 'sres-wrap'})
            search_results = [(i.attrs['href'], dom_parser.parse_dom(i, 'h2')[0].content) for i in search_results]
            search_results = [i[0] for i in search_results if cleantitle.get(i[1]) in t]

            if len(search_results) > 0:
                return source_utils.strip_domain(search_results[0])
            else:
                raise Exception()
        except:

            return
