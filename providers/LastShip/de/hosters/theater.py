# -*- coding: utf-8 -*-

import base64
import json
import re
from binascii import unhexlify

from providerModules.LastShip import cache
from providerModules.LastShip import client
from providerModules.LastShip import cleantitle
from providerModules.LastShip import dom_parser
from providerModules.LastShip import source_utils
from providerModules.LastShip import utils


class source:
    def __init__(self):
        self.priority = 1
        self.language = ['de']
        self.domains = ['theater.to']
        self.base_link = 'https://theater.to/'
        self.search = self.base_link + 'search?q=%s'

    def movie(self, imdb, title, localtitle, aliases, year):
        try:
            return self.__search([localtitle, title] + aliases)
        except:
            return

    def tvshow(self, imdb, tvdb, tvshowtitle, localtvshowtitle, aliases, year):
        try:
            return self.__search([localtvshowtitle, tvshowtitle] + aliases)
        except:
            return

    def episode(self, url, imdb, tvdb, title, premiered, season, episode):
        try:
            if not url:
                return
            result = cache.get(client.request, 48, self.base_link + url)
            links = dom_parser.parse_dom(result, 'div', attrs={'class': 'season'})
            links = [i for i in links if 'Staffel: ' + str(season) in i.content]
            episodes = dom_parser.parse_dom(links[0], 'a')

            return source_utils.strip_domain([i.attrs['href'] for i in episodes if i.content == str(episode)][0])
        except:

            return

    def sources(self, url, hostDict, hostprDict):
        sources = []
        try:
            if not url:
                return sources
            result = cache.get(client.request, 48, self.base_link + url)

            link = dom_parser.parse_dom(result, 'iframe')[0].attrs['src']

            valid, hoster = source_utils.is_host_valid(link, hostDict)

            if valid:
                sources.append({'source': hoster, 'quality': 'HD', 'language': 'de', 'url': link, 'direct': False, 'debridonly': False})

            if len(sources) == 0:
                raise Exception()
            return sources
        except:

            return sources

    def resolve(self, url):
        return url

    def __search(self, titles):
        try:
            t = [cleantitle.get(i) for i in set(titles) if i]

            for title in titles:
                result = cache.get(client.request, 48, self.search % title)

                links = dom_parser.parse_dom(result, 'div', attrs={'class': 'latest-movie-img-container'})
                links = dom_parser.parse_dom(links, 'a')
                links = [(i.attrs['href'], i.content) for i in links if 'class' not in i.attrs]
                links = [i[0] for i in links if cleantitle.get(i[1]) in t]

                if len(links) > 0:
                    return source_utils.strip_domain(links[0])
            return
        except:
            return
