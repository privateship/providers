# -*- coding: utf-8 -*-

"""
    Lastship Add-on (C) 2017
    Credits to Exodus and Covenant; our thanks go to their creators

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import json
import re
import urlparse

from providerModules.LastShip import cache
import cfscrape
from providerModules.LastShip import cleantitle
from providerModules.LastShip import dom_parser
from providerModules.LastShip import source_utils


class source:
    def __init__(self):
        self.priority = 1
        self.language = ['de']
        self.domains = ['topstreamfilm.com']
        self.base_link = 'https://topstreamfilm.com/'
        self.resolve_link = self.base_link + '?trembed=%s&trid=%s&trtype=%s'
        self.search = self.base_link + '?s=%s'
        self.scraper = cfscrape.create_scraper()

    def movie(self, imdb, title, localtitle, aliases, year):
        try:
            url = self.__search([localtitle] + aliases)
            if not url and title != localtitle: url = self.__search([title] + aliases)
            return url
        except:
            return

    def tvshow(self, imdb, tvdb, tvshowtitle, localtvshowtitle, aliases, year):
        try:
            url = self.__search([localtvshowtitle] + aliases)
            if not url and tvshowtitle != localtvshowtitle:
                url = self.__search([tvshowtitle] + aliases)
            return url
        except:
            return

    def episode(self, url, imdb, tvdb, title, premiered, season, episode):
        try:
            if not url:
                return

            data = cache.get(self.scraper.get, 3, urlparse.urljoin(self.base_link, url))

            links = dom_parser.parse_dom(data.content, 'div', attrs={'class': 'AABox'})
            links = [dom_parser.parse_dom(i, 'tr') for i in links if 'data-tab="' + season in i.content]
            links = [dom_parser.parse_dom(i, 'a')[0] for i in links[0] if 'Num">' + episode in i.content]
            return links[0].attrs['href']
        except:
            return

    def sources(self, url, hostDict, hostprDict):
        sources = []
        try:
            if not url:
                return sources

            r = cache.get(self.scraper.get, 3, urlparse.urljoin(self.base_link, url))

            link_id = re.findall('trid=(\d+)', r.content)[0]

            links = dom_parser.parse_dom(r.content, 'ul', attrs={'class': 'TPlayerNv'})
            links = dom_parser.parse_dom(links, 'li')

            for i, link in enumerate(links):
                valid, hoster = source_utils.is_host_valid('streamango.com' if i == 0 else 'openload.co', hostDict)
                if not valid: continue

                sources.append({'source': hoster, 'quality': 'HD', 'language': 'de', 'url': (i, link_id, 2 if 'episode' in url else 1), 'direct': False, 'debridonly': False})

            return sources
        except:
            return sources

    def resolve(self, url):
        i, link_id, isMovie = url
        temp_link = cache.get(self.scraper.get, 10, self.resolve_link % (i, link_id, isMovie)).content
        temp_link = dom_parser.parse_dom(temp_link, 'iframe')[0].attrs['src']
        return temp_link

    def __search(self, titles):
        try:
            t = [cleantitle.get(i) for i in set(titles) if i]

            for title in titles:
                result = self.scraper.get(self.search % title)

                links = dom_parser.parse_dom(result.content, 'ul', attrs={'class': 'MovieList'})
                links = dom_parser.parse_dom(links, 'a')
                links = [(i.attrs['href'], dom_parser.parse_dom(i, 'h3', attrs={'class': 'Title'})[0].content) for i in links]
                links = [i[0] for i in links if cleantitle.get(i[1]) in t]

                if len(links) > 0:
                    return source_utils.strip_domain(links[0])
        except:
            return
