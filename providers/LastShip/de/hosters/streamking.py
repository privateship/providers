# -*- coding: UTF-8 -*-

# Addon Name: Lastship
# Addon id: plugin.video.lastship
# Addon Provider: LastShip

import json
import re
import urllib

import urlparse

from providerModules.LastShip import cache
import cfscrape
from providerModules.LastShip import cleantitle
from providerModules.LastShip import dom_parser
from providerModules.LastShip import source_utils


class source:
    def __init__(self):
        self.priority = 1
        self.language = ['de']
        self.domains = ['streamking.eu']
        self.base_link = 'https://streamking.eu'
        self.search_link = self.base_link + '/search?q=%s'
        self.get_link = 'movie/load-stream/%s/%s?server=2'
        self.scraper = cfscrape.create_scraper()

    def movie(self, imdb, title, localtitle, aliases, year):
        try:
            titles = [localtitle] + aliases
            url = self.__search(titles)
            if not url and title != localtitle: url = self.__search([title] + aliases)

            return url
        except:
            return

    def tvshow(self, imdb, tvdb, tvshowtitle, localtvshowtitle, aliases, year):
            return

    def episode(self, url, imdb, tvdb, title, premiered, season, episode):
            return

    def sources(self, url, hostDict, hostprDict):
        sources = []

        try:
            if not url:
                return sources

            moviecontent = cache.get(self.scraper.get, 48, urlparse.urljoin(self.base_link, url))

            links = dom_parser.parse_dom(moviecontent.content, 'div', attrs={'class': 'btn-group'})
            links = dom_parser.parse_dom(links, 'a')
            links = [(i.content, i.attrs['data-video-url']) for i in links]
            quality = dom_parser.parse_dom(moviecontent, 'p')
            quality = dom_parser.parse_dom(quality, 'span', attrs={'class': 'label-primary'})

            for hoster, link in links:
                valid, hoster = source_utils.is_host_valid(hoster, hostDict)
                if not valid: continue

                sources.append({'source': hoster, 'quality': 'HD' if any(quality) else 'SD', 'language': 'de', 'url': link, 'direct': False, 'debridonly': False})

            if len(sources) == 0:
                raise Exception()
            return sources
        except:

            return sources

    def resolve(self, url):
        return url

    def __search(self, titles):
        try:
            query = self.search_link % (urllib.quote_plus(cleantitle.query(titles[0])))

            titles = [cleantitle.get(i) for i in set(titles) if i]

            searchResult = cache.get(self.scraper.get, 48, query).content

            resultTitles = dom_parser.parse_dom(searchResult, 'div', attrs={'class': 'movie-container'})
            resultTitles = dom_parser.parse_dom(resultTitles, 'div', attrs={'class': 'movie-title'})
            resultTitles = dom_parser.parse_dom(resultTitles, 'a')
            resultTitles = [(i.attrs['href'], i.content) for i in resultTitles]

            for link, title in resultTitles:
                title = cleantitle.get(title)
                if any(i in title for i in titles):
                        #We have the suspected link!
                        return source_utils.strip_domain(link)

            raise Exception()
        except:

            return
