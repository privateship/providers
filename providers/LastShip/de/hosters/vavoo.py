# -*- coding: utf-8 -*-

import base64
import json
import requests
import json

from providerModules.LastShip import source_utils
from providerModules.LastShip import cache


class source:
    def __init__(self):
        self.priority = 1
        self.language = ['de']
        self.domains = ['']
        self.base_link = base64.b64decode('aHR0cHM6Ly92amFja3Nvbi5pbmZvLw==')
        self.search_link = '/movie-search?key=%s'
        self.get_link = '/movie/getlink/%s/%s'
        self.session = requests.Session()        
        self.vavoo_auth = self._auth()
        


    def _auth(self):
        vavoo_auth_token = cache.get(self.session.post, 24, "https://www.vavoo.tv/api/box/guest",data={'reason': 'check/missing'})
        
        token=vavoo_auth_token.json()['response']['token']

        data={'service_version': '1.2.26','reason': 'check','meta_system': 'Windows','platform': 'win32', 'token': token, 'version': '2.2', 'branch': 'addonv22', 'apk_package': 'unknown', 'meta_version': '6.1.7601', 'meta_platform': 'Windows-7-6.1.7601-SP1', 'recovery_version': '1.1.7', 'processor': 'Intel64 Family 6 Model 42 Stepping 7, GenuineIntel'}

        vavoo_auth=cache.get(self.session.post, 24,"https://www.vavoo.tv/api/box/ping2",data=data)
        
        vavoo_auth=json.dumps(vavoo_auth.content)
        
        vavoo_auth=base64.b64encode(vavoo_auth)
        
        return vavoo_auth

    def movie(self, imdb, title, localtitle, aliases, year):
        try:

            content = cache.get(self.session.get,24,self.base_link + "all?type=movie&query=" + title).json()
            #content = cache.get(self.session.get,0,self.base_link + "all?type=movie&query=" + title + "&vavoo_auth=" + self.vavoo_auth).json()
                        
            d = {}
            for data in content:
                d[data["imdb"]] = data["id"]
                print(d)

            for key, value in d.iteritems():
                if key == imdb:
                    jid = str(value)
            
            content=cache.get(self.session.get,24,self.base_link + "get?locale=de&hosters=!rapidgator.net,!tata.to,!hdfilme.tv,!1fichier.com,!share-online.biz,!uploadrocket.net,!oboom.com,!rockfile.eu,!kinoger.com,!uptobox.com&resolutions=all&language=de&id=" + jid).json()
            #content=cache.get(self.session.get,0,self.base_link + "get?locale=de&hosters=!tata.to,!hdfilme.tv,!1fichier.com,!share-online.biz,!uploadrocket.net,!oboom.com,!rockfile.eu,!kinoger.com,!uptobox.com&resolutions=hd&language=de&id=" + jid + "&vavoo_auth=" + self.vavoo_auth).json()
                        
            array = content['get']['links']
            
            
            linklist = dict()
            for idx, word in enumerate(array):
                link = self.base_link + "link?hoster=" + (word['hoster']) + "&language=de&resolution=" + word['resolution'] + "&id=" + jid + "&parts=1" + "&quality=" + str(word['quality']) + "&subtitles=" + "&vavoo_auth=" + self.vavoo_auth
                # link=self.base_link+"link?hoster="+(word['hoster'])+"&language=de&resolution=hd&id="+jid+"&parts=1"+"&quality="+str(word['quality'])+"&subtitles="+"&season="+season+"&episode="+episode
                linklist[link] = word['resolution']

            urllist = dict()            

            for items, quality in linklist.iteritems():
               
                response = requests.Session().get(items)
                                
                try:
                    content = response.json()

                    dict1 = content['parts']
                    link = dict1['1']

                    urllist[link] = quality

                except:
                    print "print exception"
                    
            url = urllist

            return url
        except:
            return

    def tvshow(self, imdb, tvdb, tvshowtitle, localtvshowtitle, aliases, year):
        return tvshowtitle

    def episode(self, tvshowtitle, imdb, tvdb, title, premiered, season, episode):
        try:
            
            content = cache.get(self.session.get,24,self.base_link + "all?type=serie&query=" + tvshowtitle).json()
            
            d = {}
            for data in content:
                d[data["imdb"]] = data["id"]

            for key, value in d.iteritems():
                if key == imdb:
                    jid = str(value)
            
            content=cache.get(self.session.get,24,self.base_link + "get?locale=de&hosters=!rapidgator.net,!tata.to,!hdfilme.tv,!1fichier.com,!share-online.biz,!uploadrocket.net,!oboom.com,!rockfile.eu,!kinoger.com,!uptobox.com&resolutions=all&language=de&id=" + jid + "&season=" + season + "&episode=" + episode).json()
            
            array = content['get']['links']

            linklist = dict()
            for idx, word in enumerate(array):
                # enable resolution to retrieve all possible links, however quality checkmissing
                link = self.base_link + "link?hoster=" + (word['hoster']) + "&language=de&resolution=" + word['resolution'] + "&id=" + jid + "&parts=1" + "&quality=" + str(word['quality']) + "&subtitles=" + "&season=" + season + "&episode=" + episode + "&vavoo_auth=" + self.vavoo_auth
                # link=self.base_link+"link?hoster="+(word['hoster'])+"&language=de&resolution=hd&id="+jid+"&parts=1"+"&quality="+str(word['quality'])+"&subtitles="+"&season="+season+"&episode="+episode
                linklist[link] = word['resolution']
            
            urllist = dict()
            for items, quality in linklist.iteritems():

                response = requests.Session().get(items)

                try:
                    content = response.json()

                    dict1 = content['parts']
                    link = dict1['1']

                    urllist[link] = quality

                except:
                    print "print vavoo exception"
                    
            url = urllist

            return url
        except:
            return

    def sources(self, url, hostDict, hostprDict):
        sources = []

        try:
            if not url:
                return sources

            for items, quality in url.iteritems():
                
                if "clip" in items:
                    sources.append({'source': "clipboard.cc", 'quality': quality.upper(), 'language': 'de', 'url': items, 'direct': True, 'debridonly': False})

                valid, host = source_utils.is_host_valid(items, hostDict)
                if not valid: continue

                
                if "original" in quality:
                    quality = "SD"
                sources.append({'source': host, 'quality': quality.upper(), 'language': 'de', 'url': items, 'direct': False, 'debridonly': False, 'checkquality': True})

            return sources
        except:
            return sources

    def resolve(self, url):
        return url
