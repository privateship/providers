# -*- coding: utf-8 -*-

"""
    Lastship Add-on (C) 2019
    Credits to Placenta and Covenant; our thanks go to their creators

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""


import re
import urllib
import urlparse

try:
    from providerModules.LastShip import cache
    from providerModules.LastShip import cleantitle
    from providerModules.LastShip import dom_parser
    from providerModules.LastShip import source_utils
    from providerModules.LastShip import client
except:
    from resources.lib.modules import cache
    from resources.lib.modules import cleantitle
    from resources.lib.modules import dom_parser
    from resources.lib.modules import source_utils
    from resources.lib.modules import client


class source:
    def __init__(self):
        self.priority = 1
        self.language = ['de']
        self.domains = ['filmpalast.to']
        self.base_link = 'https://filmpalast.to'
        self.search_link = '/search/title/%s'
       
    def movie(self, imdb, title, localtitle, aliases, year):
        try:
            url = self.__search(False, [localtitle] + source_utils.aliases_to_array(aliases), False)
            if not url and title != localtitle:
                url = self.__search(False, [title] + source_utils.aliases_to_array(aliases), False)
            if not url:
                url = self.__search(False, [title] + source_utils.aliases_to_array(aliases), True)
            return url
        except:
            return

    def tvshow(self, imdb, tvdb, tvshowtitle, localtvshowtitle, aliases, year):
        try:
            url = self.__search(True, [localtvshowtitle] + source_utils.aliases_to_array(aliases), False)
            if not url and tvshowtitle != localtvshowtitle:
                url = self.__search(True, [tvshowtitle] + source_utils.aliases_to_array(aliases), False)
            return url
        except:
            return


    def episode(self, url, imdb, tvdb, title, premiered, season, episode):
        try:
            if not url:
                return

            episode = '0' + episode if int(episode) < 10 else episode
            season = '0' + season if int(season) < 10 else season

            url = re.findall('(.*?)s\d', url)[0] + 's%se%s' % (season, episode)
            return url
        except:
            return

    def sources(self, url, hostDict, hostprDict):
        sources = []
        try:
            if not url:
                return sources

            query = urlparse.urljoin(self.base_link, url)
            moviecontent = client.request(query)

            quality = dom_parser.parse_dom(moviecontent, 'span', attrs={'id': 'release_text'})[0].content.split('&nbsp;')[0]
            quality, info = source_utils.get_release_quality(quality)

            r = dom_parser.parse_dom(moviecontent, 'ul', attrs={'class': 'currentStreamLinks'})
            r = [(dom_parser.parse_dom(i, 'p', attrs={'class': 'hostName'}), dom_parser.parse_dom(i, 'a')) for i in r]
            r = [(re.sub(' hd$', '', i[0][0].content.lower()), i[1][0].attrs['data-player-url'] if 'data-player-url' in i[1][0].attrs else i[1][0].attrs['href']) for i in r if i[0] and i[1]]

            for hoster, link in r:
                valid, hoster = source_utils.is_host_valid(hoster, hostDict)
                if valid:
                    sources.append({'source': hoster, 'quality': quality, 'language': 'de', 'url': link, 'info': ', '.join(info), 'direct': False, 'debridonly': False})

            if len(sources) == 0:
                raise Exception()
            return sources
        except:

            return sources

    def resolve(self, url):
        return url

    def __search(self, isSerieSearch, titles, isTitleClean):
        try:
            t = [cleantitle.get(i) for i in set(titles) if i]
            if isTitleClean:
                t = [cleantitle.get(self.titleclean(i)) for i in set(titles) if i]

            if t == []: return

            for title in titles:
                if isTitleClean:
                    title = self.titleclean(title)
                query = self.search_link % (urllib.quote_plus(title))
                query = urlparse.urljoin(self.base_link, query)

                r = client.request(query)
                r = dom_parser.parse_dom(r, 'article')
                r = dom_parser.parse_dom(r, 'a', attrs={'class': 'rb'}, req='href')
                r = [(i.attrs['href'], i.content) for i in r]

                if len(r) > 0:
                    if isSerieSearch:
                        for i in r:
                            link = re.findall('(.*?)S\d', i[1])
                            if link == []: continue
                            if cleantitle.get(link[0]) in t: return source_utils.strip_domain(i[0])
                    else:
                        r = [i[0] for i in r if cleantitle.get(i[1]) in t ]
                        if len(r) > 0: return source_utils.strip_domain(r[0])

            return
        except:

            return

    def titleclean(self, title):
        if title is None:
            return
        for i in (':', '!', '!', '!'):  # '!' Platzhalter für weiteres
            if i in title:
                title = title.replace(i, '')
                return title
        if 'II' in title:
            title = title.replace('II', '2')
        elif 'III' in title:
            title = title.replace('III', '3')
        elif 'IV' in title:
            title = title.replace('IV', '4')

        return title