# -*- coding: utf-8 -*-

"""
    Lastship Add-on (C) 2017
    Credits to Exodus and Covenant; our thanks go to their creators

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import re
import urllib
import urlparse

from providerModules.LastShip import cache
from providerModules.LastShip import cleantitle
from providerModules.LastShip import client
from providerModules.LastShip import dom_parser
from providerModules.LastShip import source_utils


class source:
    def __init__(self):
        self.priority = 1
        self.language = ['de']
        self.domains = ['movie-stream.eu']
        self.base_link = 'https://movie-stream.eu/'
        self.search = self.base_link + 'search?q='

    def movie(self, imdb, title, localtitle, aliases, year):
        try:
            return self.__search([localtitle, title] + aliases)
        except:
            return

    def tvshow(self, imdb, tvdb, tvshowtitle, localtvshowtitle, aliases, year):
        try:
            return self.__search([localtvshowtitle, tvshowtitle] + aliases)
        except:
            return

    def episode(self, url, imdb, tvdb, title, premiered, season, episode):
        try:
            if not url:
                return

            return url, season, episode
        except:
            return

    def sources(self, url, hostDict, hostprDict):
        sources = []
        try:
            if not url:
                return sources
            season = None

            if isinstance(url, tuple):
                url, season, episode = url

            content = cache.get(client.request, 48, urlparse.urljoin(self.base_link, url))
            links = dom_parser.parse_dom(content, 'div', attrs={'class': 'season'})

            if season is None:
                links = dom_parser.parse_dom(links, 'a')
            else:
                links = [dom_parser.parse_dom(link, 'a')[int(episode) - 1] for link in links if 'Staffel %s |' % season in dom_parser.parse_dom(link, 'p')[0].content]

            quali = re.findall('class="badge badge-secondary"><font size="5px">(.*?)<', content)[0]

            for link in links:
                content = cache.get(client.request, 8, link.attrs['href'])
                hoster = dom_parser.parse_dom(content, 'iframe')[0].attrs['src']

                valid, host = source_utils.is_host_valid(hoster, hostDict)
                if not valid: continue
                sources.append({'source': host, 'quality': quali, 'language': 'de', 'url': hoster, 'direct': False, 'debridonly': False})

            if len(sources) == 0:
                raise Exception()
            return sources
        except:

            return sources

    def resolve(self, url):
        return url

    def __search(self, titles):
        try:
            t = [cleantitle.get(i) for i in set(titles) if i]

            for title in titles:
                content = cache.get(client.request, 8, self.search + urllib.quote_plus(title))

                result = dom_parser.parse_dom(content, 'div', attrs={'class': 'movie-img'})
                result = dom_parser.parse_dom(result, 'h3')
                result = dom_parser.parse_dom(result, 'a')
                result = [(i.attrs['href'], i.content) for i in result]
                links = [i[0] for i in result if cleantitle.get(i[1]) in t]
                if len(links) > 0:
                    return source_utils.strip_domain(links[0])
        except:
            return