# -*- coding: utf-8 -*-

import urllib
import urlparse

from providerModules.LastShip import cache
from providerModules.LastShip import cleantitle
from providerModules.LastShip import client
from providerModules.LastShip import dom_parser
from providerModules.LastShip import source_utils


class source:
    def __init__(self):
        self.priority = 1
        self.language = ['de']
        self.domains = ['kino.cx']
        self.base_link = 'https://kino.cx'
        self.search_link = '?s=%s'
        self.get_hoster = 'wp-admin/admin-ajax.php'

    def movie(self, imdb, title, localtitle, aliases, year):
        try:
            titles = [localtitle] + aliases
            url = self.__search(titles, year)
            if not url and title != localtitle: url = self.__search([title] + aliases, year)
            return url
        except:
            return

    def tvshow(self, imdb, tvdb, tvshowtitle, localtvshowtitle, aliases, year):
            return

    def episode(self, url, imdb, tvdb, title, premiered, season, episode):
            return

    def sources(self, url, hostDict, hostprDict):
        sources = []

        try:
            if not url:
                return sources

            url = urlparse.urljoin(self.base_link, url)
            result = cache.get(client.request, 48, url)

            links = dom_parser.parse_dom(result, 'div', attrs={'id': 'playeroptions'})
            links = dom_parser.parse_dom(links, 'li')
            links = [(i.attrs['data-post'], i.attrs['data-nume'], dom_parser.parse_dom(i, 'span', attrs={'class': 'server'})[0].content) for i in links if 'trailer' not in i.attrs['data-nume']]

            for post, nume, hoster in links:
                valid, hoster = source_utils.is_host_valid(hoster, hostDict)
                if not valid: continue

                sources.append({'source': hoster, 'quality': '720p' if 'unlimited' in hoster else 'SD', 'language': 'de', 'url': (post, nume), 'direct': False, 'debridonly': False, 'checkquality': False})

            if len(sources) == 0:
                raise Exception()
            return sources
        except:

            return sources

    def resolve(self, url):
        post, nume = url

        url = urlparse.urljoin(self.base_link, self.get_hoster)

        params ={
            'action': 'doo_player_ajax',
            'post': post,
            'nume': nume,
            'type': 'movie'
        }

        result = client.request(url, post=params)
        url = dom_parser.parse_dom(result, 'iframe')[0].attrs['src']

        return url

    def __search(self, titles, year):
        try:
            t = [cleantitle.get(i) for i in set(titles) if i]

            for title in titles:
                query = self.search_link % (urllib.quote_plus(cleantitle.query(title)))
                query = urlparse.urljoin(self.base_link, query)

                result = cache.get(client.request, 48, query)

                entry = dom_parser.parse_dom(result, 'div', attrs={'class': 'result-item'})
                entry = [(dom_parser.parse_dom(i, 'a')[0], dom_parser.parse_dom(i, 'span', attrs={'class': 'year'})[0]) for i in entry]
                entry = [(i[0].attrs['href'], dom_parser.parse_dom(i[0], 'img')[0].attrs['alt'], i[1].content) for i in entry]
                links = [i[0] for i in entry if cleantitle.get(i[1]) in t and i[2] == year]

                if len(links) == 0:
                    entry = [i for i in entry if i[2] == year]
                    links = entry[0] if len(entry) == 1 else []

                if len(links) > 0:
                    return source_utils.strip_domain(links[0])
                raise Exception()

        except:

            return
