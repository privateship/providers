# -*- coding: utf-8 -*-
import re

import urlparse

from providerModules.LastShip import cache
from providerModules.LastShip import cleantitle
from providerModules.LastShip import client
from providerModules.LastShip import dom_parser
from providerModules.LastShip import source_utils


class source:
    def __init__(self):
        self.priority = 1
        self.language = ['de']
        self.domains = ['kinomax.to']
        self.base_link = 'https://kinomax.to/'
        self.getLink = self.base_link + '/engine/ajax/stream_load.php?name=%s&host=%s&mirror=%s'
        self.getSerie = self.base_link + '/engine/ajax/stream_host_load.php?name=%s&rel=%s&s=%s&e=%s'

    def movie(self, imdb, title, localtitle, aliases, year):
        try:
            url = self.__search([localtitle] + aliases, year)
            if not url and title != localtitle: url = self.__search([title] + aliases, year)
            return url
        except:
            return

    def tvshow(self, imdb, tvdb, tvshowtitle, localtvshowtitle, aliases, year):
        url = self.__search([localtvshowtitle] + aliases, year)
        if not url and tvshowtitle != localtvshowtitle: url = self.__search([tvshowtitle] + aliases, year)
        return url

    def episode(self, url, imdb, tvdb, title, premiered, season, episode):
        try:
            url = urlparse.urljoin(self.base_link, url)
            result = cache.get(client.request, 48, url)
            link = re.findall("EpisodRel = '(.*?)'", result)[0].replace('?', '')
            name = re.findall("StreamLink = '(.*?)'", result)[0]
            return source_utils.strip_domain(self.getSerie % (name, link, str(season), str(episode))).replace('//', '/')
        except:
            return

    def sources(self, url, hostDict, hostprDict):
        sources = []
        if not url:
            return sources

        try:
            url = urlparse.urljoin(self.base_link, url)
            result = cache.get(client.request, 48, url)
            isSerie = 'stream_host_load' in url
            append = ''

            if not isSerie:
                links = dom_parser.parse_dom(result, 'div', attrs={'id' : 'host_content'})
            else:
                links = result
                append = re.findall('(&s=.*)', url)[0]
            links = dom_parser.parse_dom(links, 'button')
            links = [(i.content, i.attrs['onclick']) for i in links]

            if not isSerie:
                links = [(re.findall('(.*?)\n', i[0])[0], re.findall("StreamLink = '(.*?)'", result)[0], re.findall('\d+', i[1])[0], re.findall('/(\d+)', i[0])[0]) for i in links]

            else:
                title = re.findall("name=(.*?)&", url)[0]
                links = [(re.findall('(.*?)\n', i[0])[0], title, re.findall('\d+', i[1])[0], re.findall('/(\d+)', i[0])[0]) for i in links]

            for hoster, title, mirror, amount in links:
                valid, hoster = source_utils.is_host_valid(hoster.replace('\r', '').replace('\t', '').replace('\n', ''), hostDict)
                if not valid: continue

                for i in range(1, int(amount)):
                    link = self.getLink % (title, mirror, i) + append
                    sources.append({'source': hoster, 'quality': 'SD', 'language': 'de', 'url': link, 'direct': False, 'debridonly': False})

            if len(sources) == 0:
                raise Exception()
            return sources
        except:

            return sources

    def resolve(self, url):
        content = client.request(url)
        link = dom_parser.parse_dom(content, 'iframe')
        link = link[0].attrs['src']
        link = 'https:' + link if link[:2] == '//' else link
        return link

    def __search(self, titles, year):
        try:
            t = [cleantitle.get(i) for i in set(titles) if i]

            for title in titles:
                params = {
                    'do': 'search',
                    'subaction': 'search',
                    'story': cleantitle.getsearch(title)
                }

                result = cache.get(client.request, 48, self.base_link, post=params, headers={'Upgrade-Insecure-Requests': 1})

                links = dom_parser.parse_dom(result, 'h3')
                links = dom_parser.parse_dom(links, 'a')
                links = [(i.attrs['href'], re.findall('(.*?)\(', i.content)[0], re.findall('\((\d+?)\)', i.content)[0]) for i in links]
                links = [i[0] for i in links if cleantitle.get(i[1]) in t and year == i[2]]

                if len(links) > 0:
                    return source_utils.strip_domain(links[0])
        except:
            return