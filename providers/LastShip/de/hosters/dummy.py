# -*- coding: utf-8 -*-
import re
import urllib
import urlparse
import cfscrape

from providerModules.LastShip import cache
from providerModules.LastShip import cleantitle
from providerModules.LastShip import client
from providerModules.LastShip import source_utils
from providerModules.LastShip import dom_parser


class source:
    def __init__(self):
        self.priority = 1
        self.language = ['de']
        self.domains = ['dummy.com']
        self.base_link = 'https://google.com'

    def movie(self, imdb, title, localtitle, aliases, year):
        return

    def tvshow(self, imdb, tvdb, tvshowtitle, localtvshowtitle, aliases, year):
        return

    def episode(self, url, imdb, tvdb, title, premiered, season, episode):
        return

    def sources(self, url, hostDict, hostprDict):
        sources = []

        try:
            if not url:
                return sources
            return sources
        except:

            return sources

    def resolve(self, url):
        return
