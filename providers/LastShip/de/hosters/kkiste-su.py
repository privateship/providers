# -*- coding: utf-8 -*-

import urllib
import urlparse
import re

from providerModules.LastShip import cache, client
from providerModules.LastShip import dom_parser
from providerModules.LastShip import source_utils
from providerModules.LastShip import cleantitle


class source:
    def __init__(self):
        self.priority = 1
        self.language = ['de']
        self.domains = ['kkiste.su']
        self.base_link = 'http://kkiste.su/'
        self.search = self.base_link + 'index.php?do=search&subaction=search&search_start=1&full_search=0&result_from=1&story=%s'

    def movie(self, imdb, title, localtitle, aliases, year):
        try:
            url = self.__search([localtitle] + aliases)
            if not url and title != localtitle:
                url = self.__search([title] + aliases)
            return url
            
        except:
            return

    def tvshow(self, imdb, tvdb, tvshowtitle, localtvshowtitle, aliases, year):
            return

    def episode(self, url, imdb, tvdb, title, premiered, season, episode):
            return

    def sources(self, url, hostDict, hostprDict):
        sources = []
        try:
            if not url:
                return sources

            sHtmlContent = cache.get(client.request, 48, urlparse.urljoin(self.base_link, url))
            link = dom_parser.parse_dom(sHtmlContent, "iframe")[0].attrs['src']
            quali = dom_parser.parse_dom(sHtmlContent, 'div', attrs={'class': 'label-1'})[0].content
            quali = quali + 'p' if 'HD' in quali else quali
            quali, info = source_utils.get_release_quality(quali)

            valid, host = source_utils.is_host_valid(link, hostDict)
            if not valid: return sources

            sources.append({'source': host, 'quality': quali, 'language': 'de', 'url': link, 'info': ', '.join(info), 'direct': False, 'debridonly': False, 'checkquality': True})

            if len(sources) == 0:
                raise Exception()
            return sources
        except:

            return sources

    def resolve(self, url):
            return url

    def __search(self, titles):
        try:
            t = [cleantitle.get(i) for i in set(titles) if i]
            url = self.search % urllib.quote_plus(titles[0])

            sHtmlContent = cache.get(client.request, 48, url)
            search_results = dom_parser.parse_dom(sHtmlContent, 'div', attrs={'id': 'dle-content'})
            search_results = dom_parser.parse_dom(search_results, 'a', attrs={'class': 'sres-wrap'})
            search_results = [(i.attrs['href'], re.findall('(.*?)\((\d+)', dom_parser.parse_dom(i, 'h2')[0].content)[0]) for i in search_results]
            search_results = [i[0] for i in search_results if cleantitle.get(i[1][0]) in t]

            if len(search_results) == 0:
                raise Exception()
            return source_utils.strip_domain(search_results[0])
            return
        except:

            return
