# -*- coding: utf-8 -*-

import urlparse

from providerModules.LastShip import cache
from providerModules.LastShip import cleantitle
from providerModules.LastShip import dom_parser
from providerModules.LastShip import client
from providerModules.LastShip import source_utils
from providerModules.LastShip.recaptcha import recaptcha_app


class source:
    def __init__(self):
        self.priority = 1
        self.language = ['de']
        self.domains = ['bs.to']
        self.base_link = 'https://burningseries.co/'
        self.search_link = self.base_link + 'andere-serien'
        self.recapInfo = ''

    def tvshow(self, imdb, tvdb, tvshowtitle, localtvshowtitle, aliases, year):
        try:
            url = self.__search([localtvshowtitle] + aliases, year)
            if not url and tvshowtitle != localtvshowtitle: url = self.__search([tvshowtitle] + aliases, year)
            return url
        except:
            return

    def episode(self, url, imdb, tvdb, title, premiered, season, episode):
        try:
            if not url:
                return
            content = cache.get(client.request, 48, urlparse.urljoin(self.base_link, url) + "/" + season)
            link = dom_parser.parse_dom(content, 'table', attrs={'class': 'episodes'})
            link = dom_parser.parse_dom(link, 'a')
            link = [i.attrs['href'] for i in link if i.content == episode]

            if len(link) == 0:
                raise Exception()
            return link[0]
        except:

            return

    def sources(self, url, hostDict, hostprDict):
        sources = []

        try:
            if not url:
                return sources

            content = cache.get(client.request, 48, urlparse.urljoin(self.base_link, url))
            links = dom_parser.parse_dom(content, 'a')
            links = [i for i in links if 'href' in i.attrs and url in i.attrs['href']]
            links = [(i.attrs['href'], i.attrs['title'].replace('HD', ''), '720p' if 'HD' in i.attrs['href'] else 'SD') for i in links if 'title' in i.attrs]

            for link, hoster, quality in links:
                valid, hoster = source_utils.is_host_valid(hoster, hostDict)
                if not valid: continue
                sources.append({'source': hoster, 'quality': quality, 'language': 'de', 'info': 'Recaptcha', 'url': link, 'direct': False, 'debridonly': False, 'captcha': True})

            if len(sources) == 0:
                raise Exception()
            return sources
        except:

            return sources

    def resolve(self, url):
        try:
            url = urlparse.urljoin(self.base_link, url)
            content = client.request(url)

            url = dom_parser.parse_dom(content, 'iframe')[0].attrs['src']

            recap = recaptcha_app.recaptchaApp()

            key = recap.getSolutionWithDialog(url, "6LeiZSYUAAAAAI3JZXrRnrsBzAdrZ40PmD57v_fs", self.recapInfo)
            print "Recaptcha2 Key: " + key
            response = None
            if key != "" and "skipped" not in key.lower():
                content = client.request(url)
                s = dom_parser.parse_dom(content, 'input', attrs={'name': 's'})[0].attrs['value']
                link = client.request(url + '?t=%s&s=%s' % (key, s), output='geturl')
                return link
            elif not response or "skipped" in key.lower():
                return
        except:
            return

    def __search(self, titles, year):
        try:
            t = [cleantitle.get(i) for i in set(titles) if i]

            content = cache.get(client.request, 48, self.search_link)

            links = dom_parser.parse_dom(content, 'div', attrs={'id': 'seriesContainer'})
            links = dom_parser.parse_dom(links, 'a')
            links = [i.attrs['href'] for i in links if cleantitle.get(i.attrs['title']) in t]

            if len(links) > 0:
                return links[0]
            else:
                raise Exception()
        except:

            return

    def setRecapInfo(self, info):
        self.recapInfo = info
